//require directive is used to load the express module/packages
//it allows us to access the methods and function in easily creating our server
const express = require('express');

// this creates an express application and stores this in a constant called app
// in layman's term, app is our server
const app = express();

// setup for allowing the server to handle data from requests
// methods used express JS are middleware
	// middleware is software that provides common services and capabilities to applications outside of what's offered in the package
app.use(express.json()); // allows the app to read json data

// for our application server to run, we need a port to listen to
const port = 3000;

// express has methods corresponding to each HTTP method
// "/" corresponds with our base URI
// localhost:3000/
// Syntax: app.httpMehod("/endpoint", (req, res) => {})
app.get('/', (req, res) => {
	// res.send uses the express JS module's method to send a response back to the client
	res.send("Hello World");
});

// this route expects to receive a POST requests at the URI /endpoint "/hello"
app.post("/hello", (req, res) =>{
	// req.body contains the contents/data of the request
	console.log(req.body)

	res.send(`Hello there! ${req.body.firstName} ${req.body.lastName}`);
});

/*
	SCENARIO:

		We want to create a simple users database that will perform CRUD operations based on the client request. The following routes should peform its functionality:
*/

	// mock database for our users
	let users = [
		{
			username: "janedoe",
			password: "jane123"
		},
		{
			username: "johnsmith",
			password: "john123"
		}
	];

	// this route expects to receive a POST request at the URI "/signup"
	// this will create a user object in the "users" array that mirrors a real world regisration:
		// All fields are required/filled out

	app.post("/signup", (req, res) => {
		console.log(req.body);

		// if contents of "req.body" with property of "username" and "password" is not empty
		if (req.body.username !== "" && req.body.password !== ""&& req.body.username !== undefined && req.body.password !== undefined) {

		// to store the req.body sent via Postman in the users array, we will use "push" method
		// user object will be saved in the users array
		users.push(req.body);

		res.send(`User ${req.body.username} successfully registered!`);
		}
		// if the username and password are not complete
		else {
			res.send("Please input BOTH username and password.")
		}
	});

	// this route expects to receive a GET request at the URI "/users"
	// this will retrieve all users
	app.get("/users", (req, res) => {
		res.send(users);
	});

	// this route expects to receive a PUT request at the URI "/change-password"
	// this will update the password of a user that matches the information provided in the client/Postman
		// we will use the "username" as the search property
	app.put("/change-password", (req, res) => {
		// creates a variable to store the message to be sent back to the client/Postman (response)
		let message;

		// create a for loop that will loop through the elements of the "users" array
		for(let i = 0; i < users.length; i++) {
			if(users[i].username === req.body.username) {
				// changes the password of the user found by the loop into the password provided in the client/Postman
			
			users[i].password = req.body.password;

			message = `User ${req.body.username}'s password has been updated.`

			// break out of the loop once the user that matches the username provided in the client/Postman is found
			break;
		}
		// if no user was found
		else {
			// changes the message to be sent back as the response
			message = "User does not exist!"
		}
	}

		// sends a response back to the client/Postman once the password has been updated or if a user is not found
		res.send(message);
	});

	// This route expects to receive a DELETE request at the URI "/delete-user"
	// This will remove the user from the array for deletion
	app.delete("/delete-user", (req, res) => {
		// creates a variable to store the message to be sent back to the client/Postman (response)
		let message;

		// create a for loop that will loop through the elements of the "users" array
		for(let i = 0; i < users.length; i++) {
			if(users[i].username === req.body.username) {
				// the splice method manipulates the array and removes the user object from "users" array based in it's index
				users.splice(users[i], 1);

			message = `User ${req.body.username} has been deleted.`

			// break out of the loop once the user that matches the username provided in the client/Postman is found
			break;
		}
		// if no user was found
		else {
			// changes the message to be sent back as the response
			message = "User does not exist!"
		}
	}

		// sends a response back to the client/Postman once the password has been updated or if a user is not found
		res.send(message);
	});

// returns a message to confirm that the server is running in the terminal
app.listen(port, () => console.log(`Server is running at port: ${port}`));